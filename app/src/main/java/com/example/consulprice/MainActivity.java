package com.example.consulprice;


import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private TextView tvName1;
    private TextView tvTemp1;
    private TextView tvHum1;
    private TextView tvPress1;
    private TextView tvName2;
    private TextView tvTemp2;
    private TextView tvHum2;
    private TextView tvPress2;
    private TextView tvName3;
    private TextView tvTemp3;
    private TextView tvHum3;
    private TextView tvPress3;
    private TextView tvName4;
    private TextView tvTemp4;
    private TextView tvHum4;
    private TextView tvPress4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.tvName1 = (TextView) findViewById(R.id.tvName1);
        this.tvTemp1 = (TextView) findViewById(R.id.tvTemp1);
        this.tvHum1 = (TextView) findViewById(R.id.tvHum1);
        this.tvPress1 = (TextView) findViewById(R.id.tvPress1);
        this.tvName2 = (TextView) findViewById(R.id.tvName2);
        this.tvTemp2 = (TextView) findViewById(R.id.tvTemp2);
        this.tvHum2 = (TextView) findViewById(R.id.tvHum2);
        this.tvPress2 = (TextView) findViewById(R.id.tvPress2);
        this.tvName3 = (TextView) findViewById(R.id.tvName3);
        this.tvTemp3 = (TextView) findViewById(R.id.tvTemp3);
        this.tvHum3 = (TextView) findViewById(R.id.tvHum3);
        this.tvPress3 = (TextView) findViewById(R.id.tvPress3);
        this.tvName4 = (TextView) findViewById(R.id.tvName4);
        this.tvTemp4 = (TextView) findViewById(R.id.tvTemp4);
        this.tvHum4 = (TextView) findViewById(R.id.tvHum4);
        this.tvPress4 = (TextView) findViewById(R.id.tvPress4);

        String url = "http://api.openweathermap.org/data/2.5/weather?lat=-36.6066399&lon=-72.1034393&appid=c6e3260611fc29791f75bca42b0607aa&units=metric";
        StringRequest solicitud = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject nameJSON = new JSONObject(response);
                            //String name = nameJSON.getString("name");

                            //main es el que contiene los datos en la api
                            JSONObject clima = new JSONObject(nameJSON.getString("main"));

                            Double temperatura = Double.parseDouble(clima.getString("temp"));
                            tvTemp1.setText(" La T° es : " + temperatura + "°C");

                            Double humedad = Double.parseDouble(clima.getString("humidity"));
                            tvHum1.setText(" La humedad es : " + humedad);

                            Double presion = Double.parseDouble(clima.getString("pressure"));
                            tvPress1.setText(" La presión es : " + presion);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        String url2 = "http://api.openweathermap.org/data/2.5/weather?lat=40.4165001&lon=-3.7025599&appid=c6e3260611fc29791f75bca42b0607aa&units=metric";
        StringRequest solicitud2 = new StringRequest(
                Request.Method.GET,
                url2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject nameJSON = new JSONObject(response);

                            //main es el que contiene los datos en la api
                            JSONObject clima2 = new JSONObject(nameJSON.getString("main"));

                            Double temperatura2 = Double.parseDouble(clima2.getString("temp"));
                            tvTemp2.setText(" La T° es : " + temperatura2 + "°C");

                            Double humedad2 = Double.parseDouble(clima2.getString("humidity"));
                            tvHum2.setText(" La humedad es : " + humedad2);

                            Double presion2 = Double.parseDouble(clima2.getString("pressure"));
                            tvPress2.setText(" La presión es : " + presion2);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        String url3 = "http://api.openweathermap.org/data/2.5/weather?lat=48.8534088&lon=2.3487999&appid=c6e3260611fc29791f75bca42b0607aa&units=metric";
        StringRequest solicitud3 = new StringRequest(
                Request.Method.GET,
                url3,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject nameJSON = new JSONObject(response);

                            //main es el que contiene los datos en la api
                            JSONObject clima3 = new JSONObject(nameJSON.getString("main"));

                            Double temperatura3 = Double.parseDouble(clima3.getString("temp"));
                            tvTemp3.setText(" La T° es : " + temperatura3 + "°C");

                            Double humedad3 = Double.parseDouble(clima3.getString("humidity"));
                            tvHum3.setText(" La humedad es : " + humedad3);

                            Double presion3 = Double.parseDouble(clima3.getString("pressure"));
                            tvPress3.setText(" La presión es : " + presion3);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        String url4 = "http://api.openweathermap.org/data/2.5/weather?lat=55.7522202&lon=37.6155586&appid=c6e3260611fc29791f75bca42b0607aa&units=metric";
        StringRequest solicitud4 = new StringRequest(
                Request.Method.GET,
                url4,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject nameJSON = new JSONObject(response);

                            //main es el que contiene los datos en la api
                            JSONObject clima4 = new JSONObject(nameJSON.getString("main"));

                            Double temperatura4 = Double.parseDouble(clima4.getString("temp"));
                            tvTemp4.setText(" La T° es : " + temperatura4 + "°C");

                            Double humedad4 = Double.parseDouble(clima4.getString("humidity"));
                            tvHum4.setText(" La humedad es : " + humedad4);

                            Double presion4 = Double.parseDouble(clima4.getString("pressure"));
                            tvPress4.setText(" La presión es : " + presion4);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
        listaEspera.add(solicitud);
        listaEspera.add(solicitud2);
        listaEspera.add(solicitud3);
        listaEspera.add(solicitud4);
    }
}



